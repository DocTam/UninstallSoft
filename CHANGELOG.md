# 更新日志

### 版本号说明

> :rocket: **主版本号.子版本号.修复版本号.编译版本号**
>
> &#9989; **主版本号：** 主要涉及重大功能更新或改版或重建等；
>
> &#9989; **子版本号：** 主要涉及新增功能或功能调整以及优化等；
>
> &#9989; **修复版本号：** 主要涉及不影响功能完整性时的BUG修复；
>
> &#9989; **编译版本号：** 程序编译自动变更，仅作标识用途；

### [v1.2.1.1405] 2021.07.15

<p align="left"><a target="_blank" href="../../raw/14a1ac37db9d3ad179e094fdc48572fd1bd41dac/UninstallSoft.zip"><img src="https://img.shields.io/badge/下载-当前版本-green?style=flat&logo=gitlab"></a></p>

- 修复清理防火墙规则导致的360安全卫士卸载失败的错误；
- 新增对匹配指定内容的 word 文档的清理，高危操作，请慎用；

### [v1.1.0.1378] 2021.07.13

<p align="left"><a target="_blank" href="../../raw/f4bebac3da4a49cc7346757e686285cf737568ec/UninstallSoft.zip"><img src="https://img.shields.io/badge/下载-当前版本-green?style=flat&logo=gitlab"></a></p>

- 新增清理IE收藏夹以及主页设定；
- 新增对防火墙规则的清理的支持；

### [v1.1.0.1333] 2021.07.11

<p align="left"><a target="_blank" href="../../raw/ebe2da5805c052eea87a5bd115ddf1b488ea9d69/UninstallSoft.zip"><img src="https://img.shields.io/badge/下载-当前版本-green?style=flat&logo=gitlab"></a></p>

- 新增对软件卸载过程的检验，避免并行卸载引发的资源消耗以及异常问题；