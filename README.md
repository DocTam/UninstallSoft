<p align="center">
    <a name="top" target="_blank" href="#"><img src="https://img.shields.io/badge/UninstallSoft-卸载恶意推广流氓软件-green?style=flat&logo=gitlab" width="100%"/></a>
</p>
<p align="center">
    <a target="_blank" href="README.md"><img src="https://img.shields.io/badge/Docs-Latest-green?style=flat&logo=microsoftword"/></a>
    <a target="_blank" href="LICENSE.md"><img src="https://img.shields.io/badge/license-Apache v2.0-blue?style=flat&logo=apache"></a>
    <a target="_blank" href="CHANGELOG.md"><img src="https://img.shields.io/badge/changlogs-更新日志-white?style=flat&logo=appveyor"></a>
    <a target="_blank" href="../../releases"><img src="https://img.shields.io/badge/release-v1.1.0.1378-green?style=flat&logo=gitlab"></a>
</p>

----------------------------------------------------

[](# "标识符参考： :top: :checkered_flag: :microscope: :family: :gear: :rocket: &#8658; &#128200; &#128290; &#128292; &#9989; &#9203; &#9888; &#10062; &#10145;")

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/1-详情介绍-red?style=flat&logo=appveyor"/></a><a href="#top"> :top: </a>
</summary>
<p>

> &#9989; **你是否安装软件时，莫名奇妙就被捆绑安装上了一大堆不相干的软件？**
>
> &#9989; **你是否安装系统时，莫名奇妙就被安装上了一大堆推广软件？**
>
> &#9989; **你是否更新硬件驱动时，莫名其妙就被装上了好几家的“全家桶”？**
>
> &#9989; **还在为流氓捆绑安装软件烦恼吗？？？**

</p>
</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/2-特色说明-orange?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

> &#9989; **完全自动化操作（内置清理清单)~查找并卸载已安装流氓软件！**
>
> &#9989; **支持定制软件清理，详见配置文件。完全掌控每一个卸载细节。**

</p>
</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/3-演示说明-yellow?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

<img src="https://gitlab.com/DocTam/uninstallsoft/raw/dev/UninstallSoft-Demo-20210710193324.gif" title="Demo演示过程" width=100%>

</p>
</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/4-使用说明-green?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

> &#9989; [**下载本软件 _UninstallSoft.zip_ 后解压出执行程序；请自行比对文件hash，确认软件没有被篡改；**](https://gitlab.com/DocTam/uninstallsoft/raw/dev/UninstallSoft.zip)
>
> &#9989; **双击执行程序 _UninstallSoft.Exe_ 后会自动判断是否存在配置文件，若无则自动创建配置文件模板；**
>
> &#9989; **根据实际情况调整配置文件里的对应内容，默认无需修改则自动检测处理内置清理清单里的软件；**

</p>

<img src="https://gitlab.com/DocTam/uninstallsoft/raw/dev/UninstallSoft-Demo-Profile-20210710193324.jpg" title="软件属性——详细信息" width=100%>
  
<img src="https://gitlab.com/DocTam/uninstallsoft/raw/dev/UninstallSoft-Demo-Config-20210710193324.jpg" title="软件配置文件——详细说明" width=100%>

</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/5-授权许可-orange?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

> &#9989; **基于 Apache v2.0 协议授权许可**
>
> &#9989; **软件默认颁发一年使用证书，随软件更新自动延期。有效期内免费使用，无功能限制。**
>
> &#9989; **如有定制服务需求，按需评估收费。**

</p>
</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/6-文件hash检验-blue?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

> **文件名称:** UninstallSoft.Exe  
> **文件大小:** 431 KB (441,856 字节)  
> **文件版本:** 1.2.1.1405  
> **版本说明:** 修复清理防火墙规则导致的360安全卫士卸载失败的错误；新增对匹配指定内容的 word 文档的清理，高危操作，请慎用；  
> **修改时间:** 2021年07月15日，18:32:13  
> **MD5:** 75F897041B8531C01802BC635F3A4E9E  
> **SHA1:** 4A0F3C78FB786B6DC0B6CA34EFF7DD22C9EB3D72  
> **SHA256:** E661CCCE0C76F034D313CD2DACB152A51FF9CB03C323B1886DABADF9C3AB6B31  
> **SHA512:** AD78F045036C54B5BC51B388EC02D42CE64400A84ED2F0FB4BE847369912E3569200F04BD0BC096824E1B5970E166477523B8FCFFF10A056EA37AFFE7A574A56  
> **CRC32:** C0318FE2  

> **文件名称:** UninstallSoft.Exe  
> **文件大小:** 430 KB (440,320 字节)  
> **文件版本:** 1.1.0.1378  
> **版本说明:** 新增防火墙规则清理、清理IE收藏夹以及主页设定等；详见配置文件；  
> **修改时间:** 2021年07月13日，18:36:03  
> **MD5:** DB2F957060BC746C766DF277F86FD971  
> **SHA1:** 51FC05487DB3CBDEE82C99290E6A43904A2B4A59  
> **SHA256:** B01AF1BE179D7DEA4ED8AB05DDB3FB1C1A3746D2C6BCC7179B64650D2753E4FB  
> **SHA512:** F49A287283BD7503871567AADDDB0A8539388DDB21E2DDE87484636CF02E8394556211959D72205836381A30361B00311E0CB1ACA1211B76B9D1C673F310C7A1  
> **CRC32: ECE91723  

> **文件名称:** UninstallSoft.Exe  
> **文件大小:** 428 KB (438,784 字节)  
> **文件版本:** 1.1.0.1333  
> **版本说明:** 根据配置文件信息自动查找并卸载软件，支持定制扩展更多的软件；详见配置文件；  
> **修改时间:** 2021年07月10日，16:55:47  
> **MD5:** 5E6505A369E44ECBE82A03A740D9A785  
> **SHA1:** 606230EDA31694AB8EE070E479F2E8A542107E81  
> **SHA256:** 26EC8E6C543A6C2720F7A933BA5D7F9E73C924B7E85FB5216EC1A1F38BE69F83  
> **SHA512:** AFE2094C0AA937D79DE3DB4D625EF89E940215F59C367BD02C68EEC257289A3ADF70563B287EEC13F73E1FFA0BBAD2113EB660D8F75EDAC57ED62570AC1CE6F0  
> **CRC32:** EB128A47  

</p>
</details>

<details>
<summary>
     :rocket: <a href=""><img src="https://img.shields.io/badge/7-赞赏与增值服务-purple?style=flat&logo=appveyor"></a><a href="#top"> :top: </a>
</summary>
<p>

&#9989; **如果你喜欢这个软件，请赞赏支持一下吧：）**

<img src="https://gitlab.com/DocTam/uninstallsoft/raw/dev/Pay-QRCode.png" title="谭永军-收款码-集合" width=100%>

&#9989; **如果你需要增值服务（定制软件开发、互联网开发、物联网开发、数据库开发等），请与我联系哦：）**

</p>
</details>
